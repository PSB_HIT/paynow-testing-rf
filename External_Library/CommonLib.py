import os
import subprocess
import re
import errno
import math
import glob
import calendar
import datetime as dt
#from datetime import datetime, timedelta
import datetime
from datetime import date
from datetime import datetime as dt
from datetime import timedelta
#import win32com.client
#import win32ui

def JoinTwoStrings(arg1, arg2):
    return arg1 + " " + arg2

def SwapTwoStrings(arg1, arg2):
	a, b = arg1, arg2
	a, b = b, a
	return a, b

# Return today as format DD/MMM/YYYY
def GetToday():
	end_date = dt.utcnow()
	return end_date.strftime("%d/%b/%Y")

def SplitSlashDate(arg,index):
	print "entering split slash date"
	oldText = arg
	newText = oldText.split('/')
	return newText[index]

def DateDiff(arg1,arg2):
	date_format = "%d/%b/%Y"	#%b is short month
	a = dt.strptime(arg1, date_format)
	b = dt.strptime(arg2, date_format)
	delta = b - a
	return abs(delta.days)

def searchContain(searchKey, text):
    if searchKey in text:
        print searchKey+" is contained in "+text
        return True 
    else:
        print searchKey+" is NOT contained in "+text
        return False
		
#========================================================
# Get Past date in format %d/%b/%Y, for example, 20/Mar/2012
# Input: the number of days back in time from current date
#========================================================	
def GetPastDate(backdays):
	start_date = dt.utcnow() - timedelta(days=int(backdays))
	return start_date.strftime("%d/%b/%Y")

#========================================================
# Get Future date in format %d/%b/%Y, for example, 20/Mar/2012
# Input: the number of days in advance from current date
#========================================================	
def GetFutureDate(futuredays):
	start_date = dt.utcnow() + timedelta(days=int(futuredays))
	return start_date.strftime("%d/%b/%Y")

def ReadFile(filename):
	f = open(filename, 'r')
	lines = f.readline()
	return lines;

def ReadFileLength(filename):
	f = open(filename, 'r')
	lines = f.readline()
	return len(lines);

def WriteFile(filename,lines):
	f = open(filename, 'r')
	f = open(filename, 'w')
	f.writelines(lines)
	f.close()

#===========================================================================================================
# Append all files (.txt .csv and etc) availble on given location into one big file (Big.txt)
#===========================================================================================================
def AppendMultipleFile2One(location):
	str = os.listdir(location)
	for i in str:
		print i
		f = open(location + i)
		f2 = open(location + "Big.txt","a")
		for line in f.readlines():
			f2.write(line)
	
def ReplaceWordInFile(input, output):
	f = open(input)
	o = open(output,"a")
	count = 0
	while 1:
		line = f.readline()
		count = count + 1
		if count == 100: break 
		line = line.replace("[Name],[Version]","[Name],[Version],[Action]")
		line = line.replace("TSfCP_DATA,1","TSfCP_DATA,1,Delete")
		o.write(line)
	o.close()

def StringCount(arg1, arg2):
	str = arg1
	sub = arg2
	count = str.count(sub)
	return count

def SearchString(words, str):
	pos = words.find(str)
	if pos == -1:
		ans = "false"
	else:
		ans = "true"
	return ans

def trimWhiteSpace(input):
	return input.strip()

def ConvertDateFormat(dateStr, inputFormat, outputFormat):
	inputDate = dt.strptime(dateStr, inputFormat)
	return inputDate.strftime(outputFormat)

#===========================================================================================================
# Convert HTML source to UTF-8 (ASCII is python's default if not define any) before writing them to file (via 'WriteStringToFile')
#===========================================================================================================
def ConvertUnicodeString2UTF8(str):
	return str.encode("utf-8")

#===========================================================================================================
# Validate return date values (targetdate) are in the specify time interval (startdate, enddate)
#===========================================================================================================
def CheckDateInRange(startdate, enddate, targetdate):
	s_startdate = dt.strptime(startdate, "%d/%b/%Y")
	s_enddate = dt.strptime(enddate, "%d/%b/%Y")
	s_targetdate = dt.strptime(targetdate, "%d/%b/%Y")
	if ((s_targetdate <= s_enddate and s_startdate <= s_targetdate)):
		print "Data is in Range"
		return True
	else:
		print "Data is Out of Bound" 
		return False

#==============================================================		
# Create directory if not exist
#==============================================================
def CreateDir(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

#==============================================================		
# Check given path/file whether it is exist or not
# Output: True or False
#==============================================================			
def isFileExist(fileName):
    return os.access(fileName, os.F_OK)

#============================================================================		
# Get latest file avilable on given location
#============================================================================
def GetLatestFile(filePath):
	list_of_files = glob.glob(filePath + '\\*') 			# Can be more specific by adding the format such as: \\*.csv
	print "List of file is:", list_of_files
	latest_file = max(list_of_files, key=os.path.getctime)	# Latest is justify by created date/time
	return latest_file

#============================================================================		
# Write given string to path/file with 2 mode >> a=append, w=replace existing
#============================================================================
def WriteStringToFile(filePath,string,mode="a"):
        try:
            text_file = open(filePath, mode)
            text_file.write(string + "\n")

        except Exception, e:
            print e

        finally:
            text_file.close()

#============================================================================
# Read given path/file and put into string
#============================================================================
def ReadFileToString(filePath):
    try:
        string = open(filePath).read()
    except:
        print 'Fail to read file'
    return string

#=======================================================================================
# Read file and return back as list as input parameters. Support both txt and csv format
# Input: <Suite_name>.csv or .txt
#=======================================================================================
def ReadInputParameterFromFile(filePath, testname):
	fullFile = open(filePath,"r")
	f = fullFile.readlines()[1:]	#Skip header row to fix issue for 'UnicodeDecodeError'
	list_parameter = []	
	for line in f:
		if "\n" not in line:	line += ","		#Manage EOF, without '\n'
		line = line.replace("\n",",")			#Manage last parameter each line
		#print line
		if testname in line:
			for word in line:	list_parameter += line.split(',')
	if testname in list_parameter: list_parameter.remove(testname)	#Remove testcae name out of the list
	return list_parameter

#============================================================================
# Check given process whether it is still running or not
#============================================================================
def ProcessExists(proc_name):
    n=0# number of instances of the program running 
    prog=[line.split() for line in subprocess.check_output("tasklist").splitlines()]
    [prog.pop(e) for e in [0,1,2]] #useless 
    for task in prog:
        if task[0]==proc_name:
            n=n+1
    if n>0:
        return True
    else:
        return False
		
def sum(arg1, arg2):
	total = float(arg1) + float(arg2); # Here total is local variable.
	#print "Inside the function local total : ", total
	return total

def sumInteger(arg1, arg2):
	total = int(arg1) + int(arg2); # Here total is local variable.
	#print "Inside the function local total : ", total
	return total

def minus(arg1, arg2):
   total = float(arg1) - float(arg2); # Here total is local variable.
   #print "Inside the function local total : ", total
   return total

def multiply(arg1, arg2):
   total = float(arg1) * float(arg2); # Here total is local variable.
   #print "Inside the function local total : ", total
   return total

def division(arg1, arg2):
	total = float(arg1) / float(arg2);
	#print "Inside the function local total : ", total
	return total

def PercentTolerance(arg1,arg2):
	diff = abs(minus(arg1,arg2));
	percentage = division(diff,min(arg1,arg2))*100
	return round(percentage,2)

def IsExceedPercentTolerance(arg1,arg2,expectPercentage):
	actualPercentage = PercentTolerance(arg1,arg2)
	if minus(expectPercentage,actualPercentage) >= 0:
		return True
	else:
		return False

		
#============================================================================
# Send CTRL + TAB on web page to switch to next tab
# REF: https://github.com/zvodd/sendkeys-py-si/blob/master/doc/SendKeys.txt
#============================================================================
def SendKeyToWeb_CTRL_TAB():
	shell = win32com.client.Dispatch("WScript.Shell")
	wnd = win32ui.GetForegroundWindow()
	shell.AppActivate(wnd.GetWindowText())
	print "Current page is >>" + wnd.GetWindowText()
	#shell.SendKeys("^a") # CTRL+A may "select all" depending on which window's focused	
	#shell.SendKeys("{DELETE}") # Delete selected text?  Depends on context. :P
	shell.SendKeys("^{TAB}")

#============================================================================
# Send keystroke CTRL + t on web page to create new tab
#============================================================================
def SendKeyToWeb_CTRL_T():
	shell = win32com.client.Dispatch("WScript.Shell")
	wnd = win32ui.GetForegroundWindow()
	shell.AppActivate(wnd.GetWindowText())
	print "Current page is >>" + wnd.GetWindowText()
	shell.SendKeys("^t")

#============================================================================
# Send keystroke Alt + TAB to switch between 2 windows
#============================================================================
def SendKeyToWeb_ALT_TAB():
	shell = win32com.client.Dispatch("WScript.Shell")
	wnd = win32ui.GetForegroundWindow()
	shell.AppActivate(wnd.GetWindowText())
	print "Current page is >>" + wnd.GetWindowText()
	shell.SendKeys("%{TAB}")


def FindStringBetween(s, start, end):
  return (s.split(start))[1].split(end)[0]

def RemoveSpecificChar(text):
    newString = ""
    for i in text:
        if i not in "?!/;:&lt&gtbTip()\"\' ":		
            newString += i
    return newString

def ExtractFloatfromString(string):
	return re.findall(r'[\d\.\,\d]+', string)
	#return re.findall(r'[\d\.\d]+', string)


#===================================================================================
# Return test suite name, by removing unneccessary path and replaceing space with _
#===================================================================================
def GetSuiteNamefromFullPath(fullName):
	suiteName = fullName.rsplit('.',1)
	return suiteName[1].replace(" ","_")


#===================================================================================
# Compare float numbers as given precision - round up (CompareFloatAsRoundUp -> RoundUp)
#===================================================================================
def RoundUp(float_num, precision):
	r1 = math.ceil(float(float_num) * math.pow(10, float(precision)))
	r2 = math.pow(10, float(precision))
	if float(float_num) == 1.1: #only this edge case, this method handle differently!
		return float_num
	return r1 / r2

def CompareFloatAsRoundUp(float_a, float_b, precision):
	if float(RoundUp(float_a, precision)) == float(RoundUp(float_b, precision)):
		return True
	return False


#===================================================================================
# Compare float numbers as given precision - round down (CompareFloatAsRoundDown -> RoundDown)
#===================================================================================
def RoundDown(float_num,precision):
	f = str(float_num).split('.')	
	return f[0] + '.' + f[1][:int(precision)]

def CompareFloatAsRoundDown(float_a, float_b, precision):
    if RoundDown(float_a, precision) == RoundDown(float_b, precision):
        return True
    return False

#=======================================================================================
# Determines the day (number) of the week
# input format is dd/mm/yyyy, example is '16/05/2017'
#=======================================================================================
def DayOfWeek(ddmmyyyy):
	#my_date = date.today()
	#print my_date.isoweekday()
	#print dt.strptime('January 11, 2010', '%B %d, %Y').strftime('%A')
	day, month, year = (int(x) for x in ddmmyyyy.split('/'))
	ans = datetime.date(year, month, day)
	#return ans.weekday()		# where Monday is 0 and Sunday is 6
	return ans.isoweekday()		# where Monday is 1 and Sunday is 7

#=======================================================================================
# Determines the week (number) of the month
# input format is dd/mm/yyyy, example is '16/05/2017'
#=======================================================================================
def WeekOfMonth(ddmmyyyy):
	day, month, year = (int(x) for x in ddmmyyyy.split('/'))
	date = datetime.date(year,month,day)
	
	cal_object = calendar.Calendar(6)	#Calendar object. 6 = Start on Sunday, 0 = Start on Monday
	month_calendar_dates = cal_object.itermonthdates(date.year,date.month)

	day_of_week = 1
	week_number = 1

	for day in month_calendar_dates:
        #add a week and reset day of week
		if day_of_week > 7:
			week_number += 1
			day_of_week = 1
		if date == day:
			break
		else:
			day_of_week += 1
	return week_number

#=======================================================================================
# Determines start (Sun) and end (Sat) of week based on the given date
# input/output format are dd/mm/yyyy, example is '16/05/2017'
#=======================================================================================
def StartAndEndOfWeekFromGivenDate(ddmmyyyy):
	s_day, s_month, s_year = (int(x) for x in ddmmyyyy.split('/'))
	day = datetime.date(s_year, s_month, s_day)
	
	day_of_week = day.isoweekday()
	to_beginning_of_week = datetime.timedelta(days=day_of_week)
	beginning_of_week = day - to_beginning_of_week

	to_end_of_week = datetime.timedelta(days=6 - day_of_week)
	end_of_week = day + to_end_of_week
	
	return (beginning_of_week.strftime("%d/%m/%Y"), end_of_week.strftime("%d/%m/%Y"))