from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep  

#driver = webdriver.Ie()
#driver = webdriver.Firefox()
driver = webdriver.Chrome()

#driver.maximize_window()

# open given url
driver.get("https://www.google.co.th")
driver.implicitly_wait(2)

# enter search keyword and submit
elem = driver.find_element_by_name("q")
elem.send_keys("robotframework")
elem.submit()

# get the list of elements, displaying after the search
list = driver.find_elements_by_class_name("r")

# get the number of elements found
print ("found "+ str(len(list)) + " searches:\n")

i = 0
for listitem in list:
	print (listitem)
	i = i+1
	if (i>10): break

handle = driver.current_window_handle
print "Handle main  = ",handle

driver.implicitly_wait(2)

#elem = driver.find_element_by_xpath("//*[@id=\"loginMainForm\"]/a")
#elem.click()

# close browser windows
#driver.quit()